package org.training.reactive.programming;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Flux;

class FluxDemoTest {
	
	@Test
	public void testFluxDemo() {
		
		Flux<String> stringFlux = Flux.just("hi", "hello", "just", "hi")
				
				.log();
		
		stringFlux.subscribe(System.out::println);
	}
	

	@Test
	public void testFluxDemoError() {
		
		Flux<String> stringFlux = Flux.just("hi", "hello", "just", "hi")
				.concatWith(Flux.error(new RuntimeException("flux error")))
				.log();
		
		stringFlux.subscribe(System.out::println);
	}

}
