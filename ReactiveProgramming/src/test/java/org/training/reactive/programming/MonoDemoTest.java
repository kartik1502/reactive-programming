package org.training.reactive.programming;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Mono;

class MonoDemoTest {
	
	@Test
	public void testMono() {
		Mono<String> stringMono = Mono
				.just("hi mono")
				.log();
		
		stringMono.subscribe(System.out::println);
	}
	
	@Test
	public void testMonoError() {
		
		Mono<Object> stringMono = Mono
				.just("hi mono")
				.then(Mono.error(new RuntimeException("mono error"))).log();
		
		stringMono.subscribe(System.out::println);
		
	}

}
