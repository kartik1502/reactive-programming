package org.training.reactive.employee.service.exception;

import java.util.List;

public record GlobalErrorResponse(List<String> messages, String errorCode) {

}
