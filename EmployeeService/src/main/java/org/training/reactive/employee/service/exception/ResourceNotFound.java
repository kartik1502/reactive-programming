package org.training.reactive.employee.service.exception;

public class ResourceNotFound extends GlobalException{

	private static final long serialVersionUID = 1L;

	public ResourceNotFound(String errorMessage) {
		super(errorMessage, GlobalStatusCode.NOT_FOUND);
	}

}
