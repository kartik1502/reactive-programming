package org.training.reactive.employee.service.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.training.reactive.employee.service.dto.EmployeeDto;
import org.training.reactive.employee.service.dto.ResponseDto;
import org.training.reactive.employee.service.service.EmployeeService;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class EmployeeHandler {

	private final EmployeeService employeeService;
	
	public Mono<ServerResponse> registerEmployee(ServerRequest serverRequest) {
		
		Mono<EmployeeDto> request = serverRequest.bodyToMono(EmployeeDto.class);
		
		return request.flatMap(employee -> 
			ServerResponse
				.status(HttpStatus.CREATED)
				.contentType(MediaType.APPLICATION_JSON)
				.body(employeeService.registerEmployee(employee), EmployeeDto.class)
		);
	}
	
	public Mono<ServerResponse> retriveEmployees(ServerRequest serverRequest) {
		
		Flux<EmployeeDto> employees = employeeService.retriveEmployees();
		
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(employees, EmployeeDto.class);
	}
	
	public Mono<ServerResponse> updateEmployee(ServerRequest serverRequest) {
		
		Mono<EmployeeDto> request = serverRequest.bodyToMono(EmployeeDto.class);
		String employeeId = serverRequest.pathVariable("employeeId");
		
		return request.flatMap(employee -> 
				ServerResponse
					.status(HttpStatus.OK)
					.contentType(MediaType.APPLICATION_JSON)
					.body(employeeService.updateEmployee(Long.parseLong(employeeId), employee), ResponseDto.class)	);
	}
	
	public Mono<ServerResponse> retriveEmployee(ServerRequest serverRequest) {
		
		long employeeId = Long.parseLong(serverRequest.pathVariable("employeeId"));
		Mono<EmployeeDto> employee = employeeService.retriveEmployee(employeeId);
		
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(employee, EmployeeDto.class);
	}
	
	public Mono<ServerResponse> deleteEmployee(ServerRequest serverRequest) {
		
		long employeeId = Long.parseLong(serverRequest.pathVariable("employeeId"));
		Mono<ResponseDto> response = employeeService.deleteEmployee(employeeId);
		
		return ServerResponse.ok()
				.contentType(MediaType.APPLICATION_JSON)
				.body(response, ResponseDto.class);
	}
	
	
}
