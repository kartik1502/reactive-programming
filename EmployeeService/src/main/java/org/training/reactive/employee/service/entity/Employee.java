package org.training.reactive.employee.service.entity;

import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Employee {
	
	@Id
	private long employeeId;

	private String firstName;
	
	private String lastName;
	
	private String emailId;
}