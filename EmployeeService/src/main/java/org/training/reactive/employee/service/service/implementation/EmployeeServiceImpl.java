package org.training.reactive.employee.service.service.implementation;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.training.reactive.employee.service.dto.EmployeeDto;
import org.training.reactive.employee.service.dto.ResponseDto;
import org.training.reactive.employee.service.entity.Employee;
import org.training.reactive.employee.service.exception.GlobalStatusCode;
import org.training.reactive.employee.service.exception.ResourceNotFound;
import org.training.reactive.employee.service.repository.EmployeeRepository;
import org.training.reactive.employee.service.service.EmployeeService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

	private final EmployeeRepository employeeRepository;

	@Override
	public Mono<ResponseDto> registerEmployee(@Valid EmployeeDto employeeDto) {

		return employeeRepository.findByEmailId(employeeDto.emailId()).log().
				flatMap(existingEmployee -> {
					log.error("Resource already found");
					return Mono.just(new ResponseDto(GlobalStatusCode.CONFLICT, "Employee with same emailId already found"));
				})

				.switchIfEmpty(Mono.defer(() -> {
					log.info("initiating registering new employee");
					Employee employee = new Employee();
					BeanUtils.copyProperties(employeeDto, employee);
					return employeeRepository.save(employee)
							.map(saved ->{
								log.info("registered new employee");
								return new ResponseDto("200", "Employee registered successfully");
							});
				}));
	}

	@Override
	public Flux<EmployeeDto> retriveEmployees() {

		return employeeRepository.findAll().map(
				employee -> new EmployeeDto(employee.getFirstName(), employee.getLastName(), employee.getEmailId()))
				.switchIfEmpty(Flux.error(new ResourceNotFound("No Employees found")));
	}

	@Override
	public Mono<ResponseDto> updateEmployee(long employeeId, EmployeeDto employeeDto) {
		
		return employeeRepository.findById(employeeId)
				.flatMap(employee -> {
					log.info("initiating the update");
					BeanUtils.copyProperties(employeeDto, employee);
					return employeeRepository.save(employee)
					.map(updateEmployee -> {
						log.info("employee updatation successfull");
						return new ResponseDto("200", "Employee update Successfully");
					});
					
				}).switchIfEmpty(Mono.defer(() -> {
					log.error("Resource not found");
					throw new ResourceNotFound("No Such Employee found");
				}));			
	}

	@Override
	public Mono<EmployeeDto> retriveEmployee(long employeeId) {
		
		return employeeRepository.findById(employeeId)
				.map(employee -> {
					log.info("resource found");
					return new EmployeeDto(employee.getFirstName(), employee.getLastName(),
							employee.getEmailId());
				}).switchIfEmpty(
					Mono.error(new ResourceNotFound("No Such Employee found"))
				);
	}

	@Override
	public Mono<ResponseDto> deleteEmployee(long employeeId) {
		
		return employeeRepository.findById(employeeId)
				.map(employee -> {
					log.info("requested resource found");
					employeeRepository.delete(employee);
					return new ResponseDto("200", "Employee deleted successfully");
				}).switchIfEmpty(Mono.defer(() -> {
					log.error("requested resource not found");
					throw new ResourceNotFound("Employee not found");
				}));
	}

}
