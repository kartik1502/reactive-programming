package org.training.reactive.employee.service.route;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.training.reactive.employee.service.handler.EmployeeHandler;

@Configuration
public class RouteConfiguration {																																																																																																																																																																																																																																							

	@Bean
	RouterFunction<ServerResponse> routes(EmployeeHandler employeeHandler) {
		
		return RouterFunctions.nest(RequestPredicates.path("/api/v1/route/employees/"), 
				RouterFunctions.nest(RequestPredicates.accept(MediaType.APPLICATION_JSON), 
						RouterFunctions.route(RequestPredicates.method(HttpMethod.POST), employeeHandler::registerEmployee)
						.andRoute(RequestPredicates.GET("/"), employeeHandler::retriveEmployees)
						.andRoute(RequestPredicates.PUT("/{employeeId}"), employeeHandler::updateEmployee)
						.andRoute(RequestPredicates.GET("{employeeId}"), employeeHandler::retriveEmployee)
						.andRoute(RequestPredicates.DELETE("{employeeId}"), employeeHandler::deleteEmployee)));
	}
}
