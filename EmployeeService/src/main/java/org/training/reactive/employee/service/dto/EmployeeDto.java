package org.training.reactive.employee.service.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Pattern;

public record EmployeeDto(@Pattern(regexp = "[a-zA-Z ]+", message = "First name must contain only alphabets") String firstName, 
		@Pattern(regexp = "[a-zA-Z ]+", message = "Last name must contain only alphabets") String lastName, 
		@Email(message = "Please enter correct mail Id") String emailId) {

}