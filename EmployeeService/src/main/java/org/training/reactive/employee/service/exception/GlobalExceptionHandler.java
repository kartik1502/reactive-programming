package org.training.reactive.employee.service.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Override
	protected Mono<ResponseEntity<Object>> handleWebExchangeBindException(
			WebExchangeBindException ex, HttpHeaders headers, HttpStatusCode status,
			ServerWebExchange exchange) {

		return Mono.just(ResponseEntity.badRequest()
				.body(new GlobalErrorResponse(
						ex.getAllErrors().stream().map(ObjectError::getDefaultMessage).toList(), GlobalStatusCode.BAD_REQUEST)));
	}
	
	@ExceptionHandler(GlobalException.class)
	public Mono<ResponseEntity<ErrorResponse>> handleGlobalException(GlobalException exception) {
		
		return Mono.just(ResponseEntity.status(HttpStatus.valueOf(Integer.valueOf(exception.getErrorCode())))
				.body(new ErrorResponse(exception.getErrorCode(), exception.getErrorMessage())));
	}

}
