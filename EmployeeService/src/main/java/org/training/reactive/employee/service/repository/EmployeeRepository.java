package org.training.reactive.employee.service.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.training.reactive.employee.service.entity.Employee;

import reactor.core.publisher.Mono;

public interface EmployeeRepository extends R2dbcRepository<Employee, Long> {
	
	Mono<Employee> findByEmailId(String emailId);

}