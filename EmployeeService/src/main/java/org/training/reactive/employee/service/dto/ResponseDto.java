package org.training.reactive.employee.service.dto;

public record ResponseDto(String responseCode, String responseMessage) {

}
