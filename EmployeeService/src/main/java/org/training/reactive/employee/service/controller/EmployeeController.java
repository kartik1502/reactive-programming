package org.training.reactive.employee.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.reactive.employee.service.dto.EmployeeDto;
import org.training.reactive.employee.service.dto.ResponseDto;
import org.training.reactive.employee.service.service.EmployeeService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/employees")
@RequiredArgsConstructor
public class EmployeeController {

	private final EmployeeService employeeService;

	@PostMapping
	public Mono<ResponseEntity<ResponseDto>> registerEmployee(@RequestBody @Valid EmployeeDto employeeDto) {

		return employeeService.registerEmployee(employeeDto).map(response -> new ResponseEntity<>(response,
				HttpStatus.valueOf(Integer.valueOf(response.responseCode()))));
	}

	@GetMapping
	public Flux<EmployeeDto> getAllEmployees() {

		return employeeService.retriveEmployees();
	}

	@PutMapping("/{employeeId}")
	public Mono<ResponseEntity<ResponseDto>> updateEmployee(@PathVariable long employeeId, @RequestBody EmployeeDto employeeDto) {
		
		return employeeService.updateEmployee(employeeId, employeeDto)
				.map(response -> new ResponseEntity<>(response, HttpStatus.OK));
	}
	
	@GetMapping("/{employeeId}")
	public Mono<ResponseEntity<EmployeeDto>> retriveEmployee(@PathVariable long employeeId) {
		return employeeService.retriveEmployee(employeeId)
				.map(ResponseEntity::ok);
	}
	
	@DeleteMapping("/{employeeId}")
	public Mono<ResponseEntity<ResponseDto>> deleteEmployee(@PathVariable long employeeId) {
		return employeeService.deleteEmployee(employeeId)
				.map(ResponseEntity::ok);
	}
	
}
