package org.training.reactive.employee.service.exception;

public record ErrorResponse(String errorCode, String message) {

}