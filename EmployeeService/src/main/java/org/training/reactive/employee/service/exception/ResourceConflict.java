package org.training.reactive.employee.service.exception;

public class ResourceConflict extends GlobalException {

	private static final long serialVersionUID = 1L;

	public ResourceConflict(String errorMessage) {
		super(errorMessage, GlobalStatusCode.CONFLICT);
	}

	
}
