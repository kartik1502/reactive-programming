package org.training.reactive.employee.service.exception;

import lombok.Getter;

@Getter
public class GlobalException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String errorMessage;

	private final String errorCode;

	public GlobalException(String errorMessage, String errorCode) {
		super(errorMessage);
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

}
