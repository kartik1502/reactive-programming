package org.training.reactive.employee.service.service;

import org.training.reactive.employee.service.dto.EmployeeDto;
import org.training.reactive.employee.service.dto.ResponseDto;

import jakarta.validation.Valid;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EmployeeService {

	Mono<ResponseDto> registerEmployee(@Valid EmployeeDto employeeDto);

	Flux<EmployeeDto> retriveEmployees();

	Mono<ResponseDto> updateEmployee(long employeeId, EmployeeDto employeeDto);

	Mono<EmployeeDto> retriveEmployee(long employeeId);

	Mono<ResponseDto> deleteEmployee(long employeeId);

}