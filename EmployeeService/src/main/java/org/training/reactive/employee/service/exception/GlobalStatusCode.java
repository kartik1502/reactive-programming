package org.training.reactive.employee.service.exception;

public class GlobalStatusCode {
	
	private GlobalStatusCode() {}
	
	public static final String BAD_REQUEST = "400";
	
	public static final String CONFLICT = "409";

	public static final String CREATED = "201";

	public static final String NOT_FOUND = "404";

}
