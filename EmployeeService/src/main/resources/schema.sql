CREATE TABLE IF NOT EXISTS employee 
		(employee_id BIGINT NOT NULL AUTO_INCREMENT, 
        first_name VARCHAR(255), 
        last_name VARCHAR(255), 
        email_id VARCHAR(255), 
        PRIMARY KEY (employee_id));